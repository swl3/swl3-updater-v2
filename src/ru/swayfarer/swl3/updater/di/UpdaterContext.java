package ru.swayfarer.swl3.updater.di;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.crypto.CryptoUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.helper.SwconfIO;

@Data
@Accessors(chain = true)
@SuperBuilder
public class UpdaterContext {
    
    @NonNull
    public RLUtils rlUtils;
    
    @NonNull
    public ExceptionsHandler exceptionsHandler;
    
    @NonNull
    public CryptoUtils cryptoUtils;
    
    @NonNull
    public SwconfIO swconfIO;
    
    public String getHash(@NonNull FileSWL file, String alg)
    {
        return cryptoUtils.hash()
            .algorithm(StringUtils.orDefault(alg, "MD5"))
            .in(file.in())
            .asUTF8()
        ;
    }
}
