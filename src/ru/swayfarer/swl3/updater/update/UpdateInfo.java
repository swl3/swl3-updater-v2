package ru.swayfarer.swl3.updater.update;

import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.FilteringList;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IgnoreSwconf;
import ru.swayfarer.swl3.updater.IUpdaterInfo;
import ru.swayfarer.swl3.updater.di.UpdaterContext;
import ru.swayfarer.swl3.updater.update.UpdateContent.FileInfo;

/**
 * Информация об обновлении, на основании которой происходит обновление
 * @author swayfarer
 *
 */
@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UpdateInfo extends Swconf2Config implements IUpdaterInfo {

    @NonNull
    public UpdaterContext updaterContext;
	
	public FilteringList filtering = new FilteringList();
	
	/** Контент обновления */
	@Internal
	@CommentedSwconf("Update content")
	public UpdateContent content;
	
	@CommentedSwconf("Parent imports of update. This configuration will override imports if it's needed.")
	public ExtendedList<String> imports = new ExtendedList<>();
	
	@IgnoreSwconf
	public AtomicBoolean isImportsLoaded = new AtomicBoolean(false);
	
	@IgnoreSwconf
	public ILogger logger = LogFactory.getLogger();
	
	@Override
	public boolean isAcceptsFile(FileSWL root, FileSWL file)
	{
		String localPath = file.getLocalPath(root);
		return filtering.isMatches(localPath);
	}
	
	public void initImports()
	{
		if (isImportsLoaded.get())
			return;
		
		isImportsLoaded.set(true);
		
		ExtendedMap<String,FileInfo> files = this.content.files;
		
		var rlUtils = updaterContext.getRlUtils();
		var swconfIO = updaterContext.getSwconfIO();
		
		for (String importInfo : imports)
		{
			ResourceLink rlink = rlUtils.createLink(importInfo);
			
			UpdateInfo info = swconfIO.deserialize(UpdateInfo.class, rlink); 
			
			if (info != null)
				importInfo(info);
		}
		
		this.content.files.putAll(files);
	}
	
	public void importInfo(UpdateInfo info)
	{
		if (info == null)
			return;
		
		info.initImports();
		
		if (!content.hashingType.equals(info.content.hashingType))
		{
			logger.warning("Skiping import", info, "for", this, "because content hashing types are not equal: ", info.content.hashingType, content.hashingType);
			return;
		}
		
		this.filtering.addAll(info.filtering);
		this.content.files.putAll(info.content.files);
	}
	
	public boolean isAcceptsFile(String localPath)
	{
		return filtering.isMatches(localPath);
	}

	@Override
	public UpdateContent getUpdateContent()
	{
		return content;
	}
	
	/** 
	 * Получить информацию об обновлении
	 * @param root Корневая директория, с которой будет снято обновление
	 * @return Полученная информация об обновлении 
	 */
	public static UpdateContent of(UpdaterContext updaterContext, FileSWL root)
	{
		return of(updaterContext, root, "MD5");
	}
	
	/** 
	 * Получить информацию об обновлении
	 * @param root Корневая директория, с которой будет снято обновление
	 * @param hashingType Тип хэширования, по которому будут браться хэши файлов
	 * @return Полученная информация об обновлении 
	 */
	public static UpdateContent of(UpdaterContext updaterContext, FileSWL root, String hashingType)
	{
		return of(updaterContext, root, hashingType, null);
	}
	
	/** 
	 * Получить информацию об обновлении
	 * @param root Корневая директория, с которой будет снято обновление
	 * @param hashingType Тип хэширования, по которому будут браться хэши файлов
	 * @param uploader Функция-загрузчик файлов на удаленное хранилище 
	 * @return Полученная информация об обновлении 
	 */
	public static UpdateContent of(UpdaterContext updaterContext, FileSWL root, String hashingType, IFunction2<String, FileSWL, URL> uploader)
	{
		UpdateContent ret = new UpdateContent();
		ret.hashingType = hashingType;
		
		root.findAllSubfiles().parallelExStream().each((f) -> {
		    ret.add(updaterContext, root, f, hashingType, uploader, false);
		});
		
		return ret;
	}

}
