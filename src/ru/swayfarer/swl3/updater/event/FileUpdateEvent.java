package ru.swayfarer.swl3.updater.event;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.updater.update.UpdateContent.FileInfo;

@Data
@Accessors(chain = true)
public class FileUpdateEvent {

    @NonNull
    public FileSWL file;

    @NonNull
    public FileInfo fileInfo;

    @NonNull
    public ObservableProperty<Long> progress;

    public boolean isSkiped()
    {
        return progress == null;
    }

    public boolean isExistsOnRepo()
    {
        return fileInfo != null;
    }
}