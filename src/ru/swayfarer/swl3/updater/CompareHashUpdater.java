package ru.swayfarer.swl3.updater;

import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IgnoreSwconf;
import ru.swayfarer.swl3.updater.di.UpdaterContext;
import ru.swayfarer.swl3.updater.event.FileUpdateEvent;
import ru.swayfarer.swl3.updater.progress.ProgressDownloading;
import ru.swayfarer.swl3.updater.update.UpdateContent;
import ru.swayfarer.swl3.updater.update.UpdateContent.FileInfo;
import ru.swayfarer.swl3.updater.update.UpdateInfo;

/**
 * Загрузчик обновлений, сравнивающий файлы по хэшам
 * <br> Загрузчики требуют информацию об обновлении, которая указывает, как именно его производить {@link #updaterInfo}
 * @author swayfarer
 *
 */
@Data
@Accessors(chain = true)
public class CompareHashUpdater {

    @NonNull
    public UpdaterContext updaterContext;
	
    /** Информация об обновлении */
	@Internal
	public UpdateInfo updaterInfo;
	
	/** Логгер */
	@Internal 
	@IgnoreSwconf
	public ILogger logger = LogFactory.getLogger();
	
	/** Событие начала обновления файла */ 
	@IgnoreSwconf
	public IObservable<FileUpdateEvent> eventStartUpdating = Observables.createObservable();
	
	/** Событие ошибки обновления файла */ 
	@IgnoreSwconf
	public IObservable<FileUpdateEvent> eventFail = Observables.createObservable();
	
	/**
	 * Обновить текущую информацию об обновлении с Корневой директории и залить изменения на удаленное хранилище
	 * @param root Корневой файл, от которого пойдет обновление
	 * @param remoteRemover Функция, удаляющая файл с удаленного хоста 
	 * @param uploader Функция, которая выгружает файл на удаленный хост
	 */
	public void refreshRemote(FileSWL root, @NonNull IFunction1NoR<String> remoteRemover, @NonNull IFunction2<String, FileSWL, URL> uploader)
	{
		updaterInfo.initImports();
		UpdateContent remoteUpdateContent = updaterInfo.getUpdateContent();
		
		ReentrantLock lock = new ReentrantLock();
		
		// Удаляем лишнее из удаленного хранилища 
		for (FileInfo remoteFileInfo : new ArrayList<>(remoteUpdateContent.files.values()))
		{
			FileSWL localFile = root.subFile(remoteFileInfo.path);
			
			if (!updaterInfo.isAcceptsFile(remoteFileInfo.path))
				continue;
			
			boolean isMustRemove = false;
			
			if (!localFile.exists())
			{
				isMustRemove = true;
			}
			else if (remoteFileInfo.isDirectory != localFile.isDirectory())
			{
				isMustRemove = true;
			}
			
			if (isMustRemove)
			{
				lock.lock();
				remoteUpdateContent.files.remove(remoteFileInfo.path);
				lock.unlock();
				
				remoteRemover.apply(remoteFileInfo.path);
			}
		}
		
		root.findAllSubfiles().parallelExStream().each((localFile) -> {
		    String localPath = localFile.getLocalPath(root);
            FileInfo remoteFileInfo = remoteUpdateContent.files.get(localPath);
            
            if (!updaterInfo.isAcceptsFile(localPath))
                return;
            
            boolean isMustUpload = false;
            
            if (remoteFileInfo == null)
            {
                isMustUpload = true;
            }
            else if (localFile.isFile())
            {
                if (!remoteFileInfo.hash.equals(getHash(localFile, remoteUpdateContent.hashingType)))
                {
                    isMustUpload = true;
                }
            }
            
            if (isMustUpload)
            {
                if (!StringUtils.isEmpty(localPath))
                {
                    UpdateContent.FileInfo fileInfo = FileInfo.of(updaterContext, localFile, localPath, remoteUpdateContent.hashingType, uploader);
                    
                    lock.lock();
                    remoteUpdateContent.files.put(localPath, fileInfo);
                    lock.unlock();
                }
            }
		});
	}
	
	public String getHash(@NonNull FileSWL file, String alg)
	{
	    return updaterContext.getHash(file, alg);
	}
	
	/**
	 * Обновить текущую информацию об обновлении с Корневой директории и залить изменения на удаленное хранилище
	 * @param root Корневой файл, от которого пойдет обновление
	 * @param remoteRemover Функция, удаляющая файл с удаленного хоста 
	 * @param uploader Функция, которая выгружает файл на удаленный хост
	 */
	public void refreshNew(FileSWL root, IFunction1NoR<String> remoteRemover, IFunction2<String, FileSWL, URL> uploader)
	{
		UpdateContent updateContent = updaterInfo.getUpdateContent();
		
		UpdateContent actualInfo = UpdateInfo.of(updaterContext, root, updateContent.hashingType, null);
		
		UpdateContent remoteFiles = updateContent.copy();
		
		ExtendedList<String> removePrefixes = new ExtendedList<>();
		
		// Собираем папки, ставшие файлами для контента сервера 
		remoteFiles.files.exStream().each((e) -> {
			
			if (!this.updaterInfo.isAcceptsFile(e.getKey()))
				return;
			
			FileInfo remoteFileInfo = e.getValue();;
			FileInfo actualFileInfo = actualInfo.files.get(e.getKey());
			
			if (actualFileInfo != null)
			{
				if (remoteFileInfo.isDirectory && !actualFileInfo.isDirectory)
				{
					removePrefixes.add(remoteFileInfo.path);
					remoteFiles.files.remove(e.getKey());
				}
				else if (!remoteFileInfo.isDirectory && actualFileInfo.isDirectory)
				{
					remoteRemover.apply(remoteFileInfo.path);
					remoteFiles.files.remove(e.getKey());
				}
			}
			else
			{
				remoteFiles.files.remove(e.getKey());
			}
		});
		
		// Собираем папки, ставшие файлами для контента клиента 
		actualInfo.files.exStream().each((e) -> {
			
			if (!this.updaterInfo.isAcceptsFile(e.getKey()))
				return;
			
			FileInfo remoteFileInfo = remoteFiles.files.get(e.getKey());
			FileInfo actualFileInfo = e.getValue();
			
			if (remoteFileInfo != null && actualFileInfo != null)
			{
				if (remoteFileInfo.isDirectory && !actualFileInfo.isDirectory)
				{
					// Если на сервере лежит директория, а актуальнен файл, то заносим директорию в список на удаление и стираем саму директорию из кэша
					removePrefixes.add(remoteFileInfo.path);
					remoteFiles.files.remove(e.getKey());
					
					remoteFiles.add(updaterContext, root, root.subFile(actualFileInfo.path), actualFileInfo.hashType, uploader, false);
				}
				else if (!remoteFileInfo.isDirectory && actualFileInfo.isDirectory)
				{
					// Если на сервере лежит файл, а актуальна папка, то выгружаем папку на сервер и удаляем файл
					remoteRemover.apply(remoteFileInfo.path);
					remoteFiles.add(updaterContext, root, root.subFile(actualFileInfo.path), actualFileInfo.hashType, uploader, actualFileInfo.isDirectory);
				}
			}
			
			// Если на сервере вообще ничего нет по такому пути, то добавляем 
			else if (remoteFileInfo == null && actualFileInfo != null)
			{
				remoteFiles.add(updaterContext, root, root.subFile(actualFileInfo.path), actualFileInfo.hashType, uploader, actualFileInfo.isDirectory);
			}
		});
		
		// Удаляем содержимое папок, ставших файлами
		var prefixes = removePrefixes.exStream();
		
		actualInfo.files.exStream().each((e) -> {
			
			if (!this.updaterInfo.isAcceptsFile(e.getKey()))
				return;
			
			FileInfo remoteFileInfo = remoteFiles.files.get(e.getKey());
			
			if (remoteFileInfo != null && prefixes.contains((p) -> remoteFileInfo.path.startsWith(p)))
			{
				remoteRemover.apply(remoteFileInfo.path);
			}
		});
		
		// Удаляем папки, ставшие файлами 
		removePrefixes.each((e) -> {
			
			if (!this.updaterInfo.isAcceptsFile(e))
				return;
			
			remoteRemover.apply(e);
			FileInfo actualFileInfo = updateContent.files.get(e);
			actualFileInfo.url = uploader.apply(e, root.subFile(e)).toExternalForm();
		});
		
		// Работаем с оставшимися файлами 
		actualInfo.files.each((e) -> {
			
			if (!this.updaterInfo.isAcceptsFile(e.getKey()))
				return;
			
			FileInfo remoteFileInfo = remoteFiles.files.get(e.getKey());
			FileInfo actualFileInfo = e.getValue();
			
			if (!EqualsUtils.objectEquals(remoteFileInfo.hash, false, actualFileInfo.hash))
			{
				actualFileInfo.url = uploader.apply(actualFileInfo.path, root.subFile(remoteFileInfo.path)).toExternalForm();
			}
		});
		
		this.updaterInfo.content = remoteFiles.copy();
	}
	
	
	/**
	 * Обновить директорию до актуального состояния
	 * @param root Корневая директория
	 * @param remover Функция, которая будет удалять файлы
	 */
	public void updateLocalsFromRemote(FileSWL root, IFunction2NoR<FileSWL, FileSWL> remover)
	{
		updaterInfo.initImports();
		UpdateContent updateContent = updaterInfo.getUpdateContent();
		
		root.findAllSubfiles().parallelExStream()
		.each((f) -> {
			String localPath = f.getLocalPath(root);
			
			FileInfo remoteFileInfo = updateContent.files.get(localPath);
			
			if (StringUtils.isEmpty(localPath))
				return;
			
			if (!this.updaterInfo.isAcceptsFile(localPath))
				return;
			
			if (remoteFileInfo == null)
			{
				logger.info("Removing non-existing at remove file:", localPath);
				f.remove();
			}
			else
			{
				if (remoteFileInfo.isDirectory && f.isFile())
				{
					logger.info("Removing not a directory file:", f);
					f.remove();
					f.createIfNotFoundDir();
				}
				else if (!remoteFileInfo.isDirectory && f.isDirectory())
				{
					logger.info("Removing not a file directory:", f);
					f.remove();
					f.createIfNotFound();
				}
			}
		});
		
		var files = updateContent.files.values();
		
		ExtendedStream.of(files)
		.each(fileInfo -> {
			FileSWL localFile = root.subFile(fileInfo.path);
			
			if (!this.updaterInfo.isAcceptsFile(fileInfo.path))
			{
				eventStartUpdating.next(new FileUpdateEvent(localFile, fileInfo, null));
				return;
			}
			
			if (fileInfo.isDirectory)
				localFile.createIfNotFoundDir();
			else
				localFile.createIfNotFound();
			
			if (!fileInfo.isDirectory)
			{
				boolean isMustUpdate = false;
				
				if (!localFile.exists())
				{
					logger.warning("File", fileInfo.path, "is not exists at", localFile.getAbsolutePath(), localFile.exists());
					isMustUpdate = true;
				}
				else if (localFile.exists() && !EqualsUtils.objectEqualsSome(getHash(localFile, fileInfo.hashType), fileInfo.hash))
				{
					logger.warning("File", fileInfo.path, "has not equal hash", fileInfo.hash, getHash(localFile, updateContent.hashingType));
					isMustUpdate = true;
				}
				
				if (isMustUpdate)
				{
				    updaterContext.exceptionsHandler.safe(() -> {
						var loading = new ProgressDownloading(new URL(fileInfo.url));
						var progress = loading.getProgress();
						eventStartUpdating.next(new FileUpdateEvent(localFile, fileInfo, progress));

						logger.info("Downloading to", localFile.getAbsolutePath());
						if (!loading.start(localFile))
						{
							eventFail.next(new FileUpdateEvent(localFile, fileInfo, progress));
							logger.error("Update failed for file", localFile.getAbsolutePath());
						}
						
					}, "Error while updating file", fileInfo.path);
				}
				else
				{
					eventStartUpdating.next(new FileUpdateEvent(localFile, fileInfo, null));
				}
			}
		});
	}
	
	public static CompareHashUpdater fromSwconfInfo(@NonNull UpdaterContext updaterContext, ResourceLink rlink)
	{
		UpdateInfo updaterInfo = new UpdateInfo(updaterContext);
		updaterInfo.config().load(rlink);
		CompareHashUpdater updater = new CompareHashUpdater(updaterContext);
		updater.updaterInfo = updaterInfo;
		return updater;
	}
	
}
