package ru.swayfarer.swl3.updater.progress;

import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.observable.property.ObservableProperty;

/**
 * Загрузка, отслеживающая свой прогресс
 * см. {@link IProgressLoading}
 * @author swayfarer
 *
 */
@Data
@Accessors(chain = true)
public class ProgressDownloading implements IProgressLoading {

	/** Логгер */
	@Internal
	public ILogger logger = LogFactory.getLogger();
	
	/** {@link URL}, из которого проиходит загрузка */
	@Internal
	public URL targetUrl;
	
	/** Логировать ли процесс? */
	@Internal
	public boolean isLogging = false;
	
	/** Максимальное кол-во загружаемых байт */
	@Internal
	public int maxBytes;
	
	/** Событие создания соединения (На этом этапе можно кастомно сконфигурировать соединение) */
	@Internal
	public IObservable<URLConnection> eventCreareConnection = Observables.createObservable();
	
	/** {@link ObservableProperty}, содержащая прогресс загрузки */
	@Internal
	public ObservableProperty<Long> progress = Observables.createProperty(0l);
	
	/**
	 * Конструктор
	 * @param targetUrl Целевой {@link URL}, из которого будет производиться загрузка
	 */
	public ProgressDownloading(URL targetUrl)
	{
		this.targetUrl = targetUrl;
	}
	
	@Override
	public boolean start(OutputStream saveTarget, boolean isCloseOut)
	{
		try
		{	
			URLConnection connection = targetUrl.openConnection();
			eventCreareConnection.next(connection);
			
			maxBytes = connection.getContentLength();
			
			var dis = DataInStream.of(connection.getInputStream());
			var dos = DataOutStream.of(saveTarget);
			progress.setValue(1l);
			StreamsUtils.copyStream(dis, dos, progress::setValue,  true, isCloseOut);
		}
		catch (Throwable e)
		{
			logger.error(e, "Error while downloading", targetUrl, "by", this);
			return false;
		}
		
		return true;
	}
}
